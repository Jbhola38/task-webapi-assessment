﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.IInterface;
using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public class ISubTaskRepo : ISubTaskInterface
    {
        TasksDbContext _context;
        public ISubTaskRepo(TasksDbContext context)
        {
            _context = context;
        }
        public void CreateSubTask(SubTask subtask)
        {

            _context.SubTasks.Add(subtask);
            _context.SaveChanges();
        }

        public List<SubtaskViewModel> GetSubtasksByTaskId(int taskId)
        {
            var list = (from x in _context.Tasks 
                        join y in _context.SubTasks
                        on x.Id equals y.TasksId
                        select new SubtaskViewModel
                        {
                            Id = x.Id,
                            TaskName = x.Name,
                            SubTaskName = y.SubTaskName,
                            SubTaskCreatedBy = y.CreatedBy,
                            SubTaskCreatedOn = DateTime.Now,
                            
                        }
                       ).ToList();
            return list;
               
        }

        public List<SubTask> GetSubTask()
        {
            return _context.SubTasks.ToList();
        }
    }
}
