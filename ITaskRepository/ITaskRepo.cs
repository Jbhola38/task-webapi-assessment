﻿using TaskApi.Context;
using TaskApi.IInterface;
using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public class ITaskRepo : ITaskInterface
    {
        TasksDbContext _context;
        public ITaskRepo(TasksDbContext context)
        {
            _context = context;
        }
        public void AddTasks(Tasks task)
        {
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }

        public void DeleteTasks(int id)
        {
            
            Tasks task=_context.Tasks.FirstOrDefault(
                x=>x.Id == id);
            if (task != null)
            {
                _context.Tasks.Remove(task);
                _context.SaveChanges();
            }
        }

        public void EditTasks(int id, Tasks tasks)
        {
            (from p in _context.Tasks
             where p.Id == id
             select p).ToList().
             ForEach(x =>
             {
                 x.Name = tasks.Name;
                 x.CreatedBy = tasks.CreatedBy;
                 
                 x.Description = tasks.Description;
                
             });
        }

        public Tasks GetTaskById(int id)
        {
            Tasks task = _context.Tasks.FirstOrDefault(
               x => x.Id == id);
            return task;
        }

        public List<Tasks> GetTasks()
        {
            return _context.Tasks.ToList();
        }
    }
}
