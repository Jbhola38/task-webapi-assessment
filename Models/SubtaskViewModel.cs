﻿namespace TaskApi.Models
{
    public class SubtaskViewModel
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        public string SubTaskName { get; set; }
        public string SubTaskCreatedBy { get; set; }
        public DateTime SubTaskCreatedOn { get; set; }
    }
}
