﻿using System.ComponentModel.DataAnnotations;

namespace TaskApi.Models
{
    public class Tasks
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set;}
        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        // Add properties
        // Id, Name, Created By, Created On, Description
    }
}
