﻿using System.ComponentModel.DataAnnotations;

namespace TaskApi.Models
{
    public class SubTask
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string SubTaskName { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        public int TasksId { get; set; }
        public Tasks? tasks { get; set; }
        // Add properties
        // Id, SubTaskName, Created By, Created On, Description,
        // TaskId

    }
}
