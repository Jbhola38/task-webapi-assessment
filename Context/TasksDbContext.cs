﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TasksDbContext:DbContext
    {
        public TasksDbContext()
        {
            
        }
        public TasksDbContext(DbContextOptions<TasksDbContext> options) : base(options) { }
        public DbSet<Tasks> Tasks { get; set;}
        public DbSet<SubTask> SubTasks { get; set;}
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasData(new User
            {
                Id = 1,
                UserName = "user1",
                Password = "12345"
            },
            new User
            {
                Id = 2,
                UserName = "user2",
                Password = "12345"
            },
            new User
            {
                Id = 3,
                UserName = "user3",
                Password = "12345"
            }
            );
        }
    }
}
