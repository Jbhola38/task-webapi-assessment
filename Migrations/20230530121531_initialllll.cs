﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskApi.Migrations
{
    /// <inheritdoc />
    public partial class initialllll : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTasks_Tasks_tasksId",
                table: "SubTasks");

            migrationBuilder.DropColumn(
                name: "TaskId",
                table: "SubTasks");

            migrationBuilder.RenameColumn(
                name: "tasksId",
                table: "SubTasks",
                newName: "TasksId");

            migrationBuilder.RenameIndex(
                name: "IX_SubTasks_tasksId",
                table: "SubTasks",
                newName: "IX_SubTasks_TasksId");

            migrationBuilder.AlterColumn<int>(
                name: "TasksId",
                table: "SubTasks",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SubTasks_Tasks_TasksId",
                table: "SubTasks",
                column: "TasksId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTasks_Tasks_TasksId",
                table: "SubTasks");

            migrationBuilder.RenameColumn(
                name: "TasksId",
                table: "SubTasks",
                newName: "tasksId");

            migrationBuilder.RenameIndex(
                name: "IX_SubTasks_TasksId",
                table: "SubTasks",
                newName: "IX_SubTasks_tasksId");

            migrationBuilder.AlterColumn<int>(
                name: "tasksId",
                table: "SubTasks",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "TaskId",
                table: "SubTasks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_SubTasks_Tasks_tasksId",
                table: "SubTasks",
                column: "tasksId",
                principalTable: "Tasks",
                principalColumn: "Id");
        }
    }
}
