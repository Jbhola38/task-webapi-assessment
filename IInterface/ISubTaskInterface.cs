﻿using TaskApi.Models;

namespace TaskApi.IInterface
{
    public interface ISubTaskInterface
    {
        public void CreateSubTask(SubTask subtask);
        public List<SubtaskViewModel> GetSubtasksByTaskId(int taskId);
        public List<SubTask> GetSubTask();
    }
}
