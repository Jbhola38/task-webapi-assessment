﻿using TaskApi.Models;

namespace TaskApi.IInterface
{
    public interface ITaskInterface
    {
        public List<Tasks> GetTasks();
        public Tasks GetTaskById(int id);
        public void AddTasks(Tasks task);
        public void EditTasks(int id,Tasks tasks);
        public void DeleteTasks(int id);
    }
}
