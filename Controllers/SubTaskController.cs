﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskApi.IInterface;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubTaskController : ControllerBase
    {
        // Add required dependencies here
        //public async Task<IActionResult> GetAllSubtasks()
        //{

        //}
        ISubTaskInterface _repo;
        public SubTaskController(ISubTaskInterface repo)
        {
            _repo = repo;
        }
        [HttpGet("{id}")]
        public ActionResult<List<SubTask>> GetSubTask(int id)
        {
            if (_repo.GetSubtasksByTaskId(id).ToList().Count == 0)
            {
                return NotFound("There are no Records");
            }
            else
            {
                return Ok(_repo.GetSubtasksByTaskId(id).ToList());
            }
        }
        [HttpPost]
        public ActionResult<int> CreateSubTask(SubTask subtask)
        {
            _repo.CreateSubTask(subtask);
            return Created("Created", subtask);
        }

    }
}
